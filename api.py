from flask import Flask, request, jsonify
import json
import sqlite3

app=Flask(__name__)

# std_list=[
#     {
#         "id":1,
#         "name":"vetri",
#         "department":"mca"

#     },
#     {
#         "id":2,
#         "name":"mani",
#         "department":"mba"

#     },
#     {
#         "id":3,
#         "name":"siva",
#         "department":"be"

#     }
# ]



def db_connection():
    conn=None
    try:
        conn=sqlite3.connect("student.sqlite")
    except sqlite3.error as e:
        print(e)
    return conn


@app.route('/student',methods=['GET','POST'])
def db():
    conn = db_connection()
    cursor=conn.cursor()


    if request.method =='GET': 
        cursor = conn.execute("select * from STUDENT")
        std_list=[dict(id=row[0],name=row[1],department=row[2]) for row in cursor.fetchall()]
        if std_list is not None:
            return jsonify(std_list)
       

    if request.method == 'POST':
        new_name=request.form['name']
        new_department=request.form['department']
        #iD=std_list[-1]['id']+1
        sql="""insert into STUDENT (name,department) values (?,?)"""
        cursor=conn.execute(sql,(new_name,new_department))
        conn.commit()
        # new_obj={
        #     "id":iD,
        #     "name":new_name,
        #     "department":new_department
        # } 
        # std_list.append(new_obj)
        # return jsonify(std_list),201
        return f"student with the id:{cursor.lastrowid} created successfully"

# @app.route('/id/<int:id>',methods=['GET','PUT','DELETE'])
# def single_row(id):
#     if request.method=="GET":
#         for std in std_list:
#             if std['id']==id:
#                 return jsonify(std)
#             pass 

#     if request.method=='PUT':
#         for std in std_list:
#             if std['id']==id:
#                 std['name']=request.form['name']
#                 std['department']=request.form['department']
#                 updated_std={
#                     'id':id,
#                     'name':std['name'],
#                     'department':std['department']
#                 }

#     if request.method=='DELETE':
#         for index,std in enumerate(std_list):
#             if std['id']==id:
#                 std_list.pop(index)
#                 return jsonify(std_list)


@app.route('/id/<int:id>',methods=['GET','PUT','DELETE'])
def single_row(id):
    conn=db_connection()
    cursor=conn.cursor
    student=None
    if request.method == 'GET':
        cursor.execute("select * from STUDENT where id=?",(id,))
        rows=cursor.fetchall()
        for r in rows:
            student=r
        if student is not None:
            return jsonify(student),200
        else:
            return "something went wrong",404
    
    if request.method=='PUT':
        sql="""update STUDENT SET  name=?,department=? where id=? """
        name=request.form['name']
        department=request.form['department']
        updated_std={
                    'id':id,
                    'name':name,
                    'department':department
                }
        conn.execute(sql,(name,department,id))
        conn.commit()
        return jsonify(updated_std)

    if request.method=='DELETE':
        sql="""delete from STUDENT where id=?"""
        conn.execute(sql,(id,))
        conn.commit()
        return "the student with id:{} has been deleted".format(id),200





if __name__=='__main__':
    app.run(debug=True)
